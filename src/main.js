import Vue from 'vue'
import App from './App.vue'
import router from './router.js'
import './plugins/element.js'
import "element-ui/lib/theme-chalk/index.css"
import './assets/css/global.css'

import axios from 'axios'
// 挂载,所有组件都能访问到http
Vue.prototype.$http = axios
// 配置请求根路径
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
