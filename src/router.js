import Vue from 'vue'
import Router from 'vue-router'
import User_Login from './components/User_Login'
import Home from "../views/Home"

Vue.use(Router)

const router = new Router({
  routes:[
    {path: '/',redirect: '/login'}, // 重定向
    {path: '/login',component: User_Login},
    {path: '/home', component: Home}
  ]
})

// 设置访问权限(挂载路由导航守卫)
router.beforeEach((to,from,next)=>{
  if(to.path === '/login') return next()
  if(!window.sessionStorage.getItem('token')) return next('/login')
  next()
})

export default router
